package Question11;

import java.util.ArrayList;

public class TestAnimal {

	public static void main(String[] args) {
		ArrayList<Animal> animals = new ArrayList<>();
		
		Cat cat = new Cat("Tom");
		animals.add(cat);
		
		Dog dog = new Dog("Scooby Doo");
		animals.add(dog);
		
		Duck duck = new Duck("Donald");
		animals.add(duck);
		
		for (Animal animal : animals){
			System.out.println(animal.getName() + " , ");
		}
		
		for (Animal animal : animals){
			System.out.println(animal.speak() + " , ");
		}
	}

}
