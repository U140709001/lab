package midterm;

public class Question2 {

	public static void main(String[] args) {
		
        System.out.println(max(6,5));
        System.out.println(max(3,4));
	}
	
	public static int max(int a, int b){
		if( a > b){
			return a;
		}
		
		return b;
	}

}
